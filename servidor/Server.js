var fs = require('fs')
    , express = require('express')
    , socketio = require('socket.io')
    , app = express()
    , server = require('http').createServer(app)
    , path = require("path")
    , cookieParser = require('cookie-parser');


app.listen(8080, function () {
    var host = 'localhost';
    var port = 8080;

    console.log("Servidor escutando em http://%s:%s", host, port);
});

app.use("/js", express.static(path.join(__dirname+'/../aplicativo/js')));
app.use("/css", express.static(path.join(__dirname+'/../aplicativo/css')));
app.use(cookieParser());


app.all('/*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.get('/', function (req, res) {
    if (req.cookies.logado) {
        res.sendFile(path.join(__dirname, '../aplicativo/index.html'));
    }
    else {
        res.sendFile(path.join(__dirname+'/../aplicativo/login.html'));
    }

});

app.get('/login', function (req, res) {
    res.cookie('logado', true);
    res.redirect('/');
});

socketio.listen(server).on('connection', function (socket) {
    socket.on('message', function (msg) {
        console.log('Message Received: ', msg);
        socket.broadcast.emit('message', msg);
    });
});